import React from 'react'

const ViewCovid = ({deaths, confirmed, recovered}) => {
    return(
        <>
        <div>Muertes: {deaths}</div>
        <div>Confirmados: {confirmed}</div>
        <div>Recuperados: {recovered}</div>
        </>
    )
}
export default ViewCovid