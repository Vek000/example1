import useFetch from '../../hooks/useFetch'
import  ViewCovid from './ViewCovid'


function Covid(){

    const {data, loading} =  useFetch('https://enrichman.github.io/covid19/world/full.json')

    if(loading){
        return(
            <p>Cargando datos...</p>
        )
    }
    
    return(
        <>
        <h1>World Covid-19</h1>
        <ViewCovid deaths={data.deaths} confirmed={data.confirmed} recovered={data.recovered}></ViewCovid>
        </>
    )
}
export default Covid